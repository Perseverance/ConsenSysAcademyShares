pragma solidity ^0.4.15;

contract Splitter {
    address owner;

    mapping(address => uint) public balanceOf;

    event ContractDestroyed(address destroyer);
    event LogFundsSent(address sender, uint funds);
    event LogFundsReceived(address sender, address receiver, uint funds);
    event LogFundsWithdrawn(address withdrawer, uint howMuch);

    modifier onlyowner() {
        require(msg.sender == owner);
        _;
    }

    function Splitter() {
        owner = msg.sender;
    }

    //You can simply call getBalance from web3, but I am putting this in order to test im remix
    function getContractBalance() constant returns(uint) {
        return this.balance;
    }

    function fund(address receiverBob, address receiverCarol) payable{
        require(msg.value > 0);

        LogFundsSent(msg.sender, msg.value);

        balanceOf[receiverBob] += msg.value/2;
        balanceOf[receiverCarol] += msg.value/2;
        balanceOf[msg.sender] += msg.value%2;

        LogFundsReceived(msg.sender, receiverBob, msg.value/2);
        LogFundsReceived(msg.sender, receiverCarol, msg.value/2);

    }

    function withdrawFunds(uint howMuch) public {
        require(balanceOf[msg.sender] >= howMuch);

        balanceOf[msg.sender] -= howMuch;
        msg.sender.transfer(howMuch);

        LogFundsWithdrawn(msg.sender, howMuch);
    }

    function closeContract() onlyowner {
        ContractDestroyed(msg.sender);
        selfdestruct(msg.sender);
    }
}
