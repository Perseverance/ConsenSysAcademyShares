pragma solidity ^0.4.15;
contract Owned {
    address owner;

    modifier onlyowner() {
        require(msg.sender == owner);
        _;
    }

    function Owned() {
        owner = msg.sender;
    }
}
