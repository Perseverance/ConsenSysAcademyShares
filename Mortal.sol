pragma solidity ^0.4.15;

import "./Owned.sol";

contract Mortal is Owned {

    event LogContractDestroyed(address destroyer);

    function closeContract() onlyowner {
        LogContractDestroyed(msg.sender);
        selfdestruct(msg.sender);
    }

}
