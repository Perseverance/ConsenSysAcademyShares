pragma solidity ^0.4.15;
import "./PaidService.sol";

contract Remittance is PaidService {

    event LogChallengeCreated(address sender, bytes32 receiver, uint deadline);
    event LogChallengeClaimed(address receiver, uint valueClaimed);
    event LogChallengeRefunded(address sender, uint valueRefunded);

    struct Challenge {
        uint challengeValue;
        uint deadline;
    }

    uint maxChallengeDuration;

    mapping(bytes32 => Challenge) public challenges;

    function Remittance(uint maxSecondsActive, uint _serviceFee) PaidService(_serviceFee) {
        maxChallengeDuration = maxSecondsActive;
    }


    //Note to self: It is inconvinient for the creator to put the ready passHash, but it is better than sending the two plain text passwords here
    //I am hiding the receiver with keccak256 hash of his address. It's a tradeoff between security and gas, though.
    function createChallenge(bytes32 receiverHash, bytes32 passHash, uint secondsActive) payable paidService {
        require(receiverHash != keccak256(address(0x0)));
        require(secondsActive > 0 && secondsActive <= maxChallengeDuration);


        bytes32 key = keccak256(msg.sender, receiverHash, passHash);

        // Making sure a passHash, hence the key is not reused
        assert((challenges[key].challengeValue == 0) && (challenges[key].deadline == 0));

        uint deadline = now + secondsActive;
        uint challangeValue = PaidService.valueAfterFees();

        challenges[key] = Challenge({challengeValue: challangeValue, deadline: deadline});

        LogChallengeCreated(msg.sender, receiverHash, deadline);
    }

    function claim(address challengeCreator, bytes32 emailPass, bytes32 smsPass) {
        bytes32 passHash = keccak256(emailPass, smsPass);
        bytes32 receiverHash = keccak256(msg.sender);
        bytes32 key = keccak256(challengeCreator, receiverHash, passHash);

        Challenge storage c = challenges[key];

        require(c.challengeValue > 0);
        require(c.deadline > now);

        uint valueToBeSent = c.challengeValue;
        c.challengeValue = 0;

        msg.sender.transfer(valueToBeSent);

        LogChallengeClaimed(msg.sender, valueToBeSent);
    }

    function refund(bytes32 receiverHash, bytes32 passHash) {
        bytes32 key = keccak256(msg.sender, receiverHash, passHash);
        Challenge storage c = challenges[key];

        require(now > c.deadline);
        require(c.challengeValue > 0);

        uint valueOwed = c.challengeValue;
        c.challengeValue = 0;

        msg.sender.transfer(valueOwed);

        LogChallengeRefunded(msg.sender, valueOwed);


    }

}
