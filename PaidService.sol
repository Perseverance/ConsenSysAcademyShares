pragma solidity ^0.4.15;
import "./Owned.sol";

contract PaidService is Owned {
    uint public serviceFee;
    uint public feesToBeCollected;

    event LogServiceFeeAccounted(address payer, uint amount);
    event LogServiceFeesCollected(uint amount);

    function PaidService(uint _serviceFee) {
        serviceFee = _serviceFee;
    }

    function valueAfterFees() internal constant returns(uint) {
        return msg.value - serviceFee;
    }

    function collectFees() onlyowner {
        uint value = feesToBeCollected;
        feesToBeCollected = 0;
        owner.transfer(value);
        LogServiceFeesCollected(value);
    }

    modifier paidService {
        require(msg.value > serviceFee);
        feesToBeCollected += serviceFee;
        LogServiceFeeAccounted(msg.sender, serviceFee);
        _;
    }

}
