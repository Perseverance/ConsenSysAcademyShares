var ShopFront = artifacts.require("./ShopFront.sol");
var ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");

module.exports = function(deployer) {
  let dummyShippingOracleAddress = "0x111";
  let sponsor = "0x1112";
  deployer.deploy(ShopFrontTokenPriceOracle).then(function() {
    return deployer.deploy(ShopFront, sponsor, dummyShippingOracleAddress, ShopFrontTokenPriceOracle.address);
  });
};
