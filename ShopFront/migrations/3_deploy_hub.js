var ShopFrontHub = artifacts.require("./ShopFrontHub.sol");

module.exports = function(deployer) {
  const serviceFee = 5000000;
  deployer.deploy(ShopFrontHub, serviceFee);
};
