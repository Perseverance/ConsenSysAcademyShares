const ShopFront = artifacts.require("./ShopFront.sol");
const ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFrontShippingOraclized', function(accounts) {

  let SFInstance;
  let SFTPOInstance;

  const _owner = accounts[0];
  const _notOwner = accounts[1];
  const _sponsor = accounts[2];
  const _shippingOracle = accounts[5];

  const transactionValue = web3.toWei(1, "ether");

  beforeEach(async function() {
    SFTPOInstance = await ShopFrontTokenPriceOracle.new();
    SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
  })

  describe("initialization", () => {
    it("should set the oracle on init", async function() {

      let oracle = await SFInstance.shippingOracle.call({ from: _sponsor });

      assert.strictEqual(oracle, _shippingOracle, "The oracle was not set correctly on init of the contract!");
    });

  })

  describe("setting oracle", () => {
    it("should be able to change oracle", async function() {
      await SFInstance.setShippingOracle(_notOwner, { from: _sponsor });

      let oracle = await SFInstance.shippingOracle.call();

      assert.strictEqual(oracle, _notOwner, "The oracle was not changed to the new oracle!");
    })

    it("should not be able to change oracle from non-sponsor", async function() {
      await expectThrow(SFInstance.setShippingOracle(_notOwner, { from: _notOwner }))
    });

    it("should emit event for oracle set", async function() {
      const expectedEvent = 'LogOracleSet';
      let result = await SFInstance.setShippingOracle(_notOwner, { from: _sponsor });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the contract closure!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    })
  })

});
