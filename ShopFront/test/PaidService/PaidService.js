const ShopFrontHub = artifacts.require("./ShopFrontHub.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFrontHubPaidService', function(accounts) {

  let SFHInstance;

  const _owner = accounts[0];
  const _notOwner = accounts[1];

  const _serviceFee = 72740350474998000;

  const transactionValue = web3.toWei(1, "ether");

  describe("initialization", () => {
    beforeEach(async function() {
      SFHInstance = await ShopFrontHub.new(_serviceFee);
    })

    it("should set service fee on init", async function() {

      let fee = await SFHInstance.serviceFee.call();

      assert(fee.eq(_serviceFee), "The service fee was not set correctly!");
    });

  })

  describe("changing service fee", () => {
    beforeEach(async function() {
      SFHInstance = await ShopFrontHub.new(_serviceFee);
    })

    it("should be able to change service fee", async function() {
      await SFHInstance.changeServiceFee(transactionValue);

      let fee = await SFHInstance.serviceFee.call();

      assert(fee.eq(transactionValue), "The service fee was not changed correctly!");
    })

    it("should not be able to change service fee from non-owner", async function() {
      await expectThrow(SFHInstance.changeServiceFee(transactionValue, { from: _notOwner }))
    });

    it("should emit event for oracle set", async function() {
      const expectedEvent = 'LogServiceFeeChanged';
      let result = await SFHInstance.changeServiceFee(transactionValue);
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from changing service fee!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    })
  })

  describe("working with fees", async function() {
    beforeEach(async function() {
      SFHInstance = await ShopFrontHub.new(_serviceFee);
      await SFHInstance.createShop("0x11", "0x22", { from: _notOwner, value: _serviceFee });
    })

    it("should increase fees to be collected", async function() {
      let fees = (await SFHInstance.feesToBeCollected.call()).toNumber();

      assert.strictEqual(fees, _serviceFee, "The feesToBeCollected is not correct");
    })

    it("should zero fees after collect", async function() {
      await SFHInstance.collectFees();
      let afterCollect = await SFHInstance.feesToBeCollected.call();

      assert.strictEqual(afterCollect.toNumber(), 0, "The fees were not reset!");
    })

    it("should be able to collect fees", async function() {
      let beforeCollect = await web3.eth.getBalance(_owner);

      await SFHInstance.collectFees();
      let afterCollect = await web3.eth.getBalance(_owner);

      assert.isAbove(afterCollect.toNumber(), beforeCollect.toNumber(), "The owner was not funded after collecting fees!");
    })

    it("should throw if non-owner tries to collect", async function() {
      await expectThrow(SFHInstance.collectFees.call({ from: _notOwner }))
    })

    it("should emit event for oracle set", async function() {
      const expectedEvent = 'LogServiceFeesCollected';
      let result = await SFHInstance.collectFees();
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from collecting service fees!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    })

  })

});
