const ShopFront = artifacts.require("./ShopFront.sol");
const ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFrontCoBuy', function(accounts) {

  let SFInstance;
  let SFTPOInstance;

  const _owner = accounts[0];
  const _notOwner = accounts[1];
  const _sponsor = accounts[2];
  const _shippingOracle = accounts[5];

  const _dummyPriceEnough = 100;
  const _dummyPriceNotEnough = _dummyPriceEnough / 4;

  const dummyProduct = {
    price: _dummyPriceEnough,
    stock: 30,
    meta: "VeryCoolProduct__Description"
  }

  const transactionValue = web3.toWei(1, "ether");

  describe("starting cobuy", () => {
    let productId;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
      productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })
    })

    it("should issue receipt when cobuy started", async function() {
      let counterBefore = await SFInstance.coBuyCounter.call();

      await SFInstance.coBuy(productId, { value: _dummyPriceNotEnough });
      let counterAfter = await SFInstance.coBuyCounter.call();

      assert(counterAfter.eq(counterBefore.plus(1)), "The receipts counter has not incremented");

    })

    it("returns new receipt for each cobuy", async function() {
      let firstReceiptId = await SFInstance.coBuy.call(productId, { value: _dummyPriceNotEnough });

      await SFInstance.coBuy(productId, { value: _dummyPriceNotEnough });
      let secondReceiptId = await SFInstance.coBuy.call(productId, { value: _dummyPriceNotEnough });

      assert.notEqual(firstReceiptId.toString(10), secondReceiptId.toString(10), "The receipts returned by the coBuy are one and the same");

    });

    it("saves receipt when cobuy started", async function() {
      let receiptId = await SFInstance.coBuy.call(productId, { value: _dummyPriceNotEnough });

      await SFInstance.coBuy(productId, { value: _dummyPriceNotEnough });

      let storedReceipt = await SFInstance.partialReceipts.call(receiptId.toNumber());

      assert(receiptId.eq(storedReceipt[0]), "The receipt id stored is different from the one returned");
      assert(storedReceipt[1].eq(_dummyPriceNotEnough), "The value accumulated stored is not correct");
      assert(storedReceipt[2].eq(productId), "The product id stored is different from the one returned");
      assert.isTrue(storedReceipt[3], "The storedReceipt is not active");

    });

    it("throws if there are no funds", async function() {
      await expectThrow(SFInstance.coBuy.call(productId, { value: 0 }));
    });
    it("throws if product not available", async function() {
      await expectThrow(SFInstance.coBuy.call(productId + 1, { value: _dummyPriceNotEnough }));
    });
    it("throws if the value is more than the price", async function() {
      await expectThrow(SFInstance.coBuy.call(productId, { value: _dummyPriceEnough }));
    });

    it("should emit an event when cobuy started", async function() {
      const expectedEvent = 'LogCoBuyStarted';
      let result = await SFInstance.coBuy(productId, { value: _dummyPriceNotEnough });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the co-buy creation!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });

  });

  describe("finishing cobuy", () => {
    let productId;
    let partialReceiptId;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
      productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor });
      partialReceiptId = (await SFInstance.coBuy.call(productId, { value: _dummyPriceNotEnough })).toNumber();
      await SFInstance.coBuy(productId, { value: _dummyPriceNotEnough });
    })

    it("should be able to contribute to cobuy from the same user", async function() {
      let result = await SFInstance.contributeToCoBuy.call(partialReceiptId, { value: _dummyPriceNotEnough })
      assert.isTrue(result[0], "It wasnt able to contribute from the same user");
    });

    it("should be able to contribute to cobuy from another user", async function() {
      let result = await SFInstance.contributeToCoBuy.call(partialReceiptId, { from: _notOwner, value: _dummyPriceNotEnough })
      assert.isTrue(result[0], "It wasnt able to contribute; from another user");
    });

    it("adds to the value accumulated on contribution", async function() {
      let storedReceipt = await SFInstance.partialReceipts.call(partialReceiptId);
      let valueContributedBefore = storedReceipt[1];

      await SFInstance.contributeToCoBuy(partialReceiptId, { from: _notOwner, value: _dummyPriceNotEnough })

      storedReceipt = await SFInstance.partialReceipts.call(partialReceiptId);
      let valueContributedAfter = storedReceipt[1];

      assert(valueContributedAfter.eq(valueContributedBefore.plus(_dummyPriceNotEnough)));
    });

    it("is able to coplete cobuy", async function() {
      let result = await SFInstance.contributeToCoBuy.call(partialReceiptId, { from: _notOwner, value: _dummyPriceEnough });
      assert.isTrue(result[1], "It wasnt able to contribute; from another user");
      assert.isAbove(result[2].toNumber(), 0, "ReceiptId is 0 or below");
    });

    it("throws if there are no funds", async function() {
      await expectThrow(SFInstance.contributeToCoBuy.call(partialReceiptId, { from: _notOwner, value: 0 }));
    });

    it("should throw if the co-buy is not active", async function() {
      await expectThrow(SFInstance.contributeToCoBuy.call(partialReceiptId + 1, { from: _notOwner, value: _dummyPriceEnough }));
    });

    it("should throw if the co-buy is not active anymore", async function() {
      await SFInstance.contributeToCoBuy(partialReceiptId, { value: _dummyPriceEnough });
      await expectThrow(SFInstance.contributeToCoBuy.call(partialReceiptId, { from: _notOwner, value: _dummyPriceEnough }));
    });

    it("throws if the product is not available anymore", async function() {
      await SFInstance.deactivateProduct(productId, { from: _sponsor });
      await expectThrow(SFInstance.contributeToCoBuy.call(partialReceiptId, { from: _notOwner, value: _dummyPriceEnough }));
    });

    it("should emit event for contribution", async function() {
      const expectedEvent = 'LogCoBuyContributed';

      let result = await SFInstance.contributeToCoBuy(partialReceiptId, { from: _notOwner, value: _dummyPriceNotEnough });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the contribution!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });

    it("should emit event for cobuy complete", async function() {
      const expectedEventContribution = 'LogCoBuyContributed';
      const expectedEventComplete = 'LogCoBuyFinished';

      let result = await SFInstance.contributeToCoBuy(partialReceiptId, { from: _notOwner, value: _dummyPriceEnough });
      assert.lengthOf(result.logs, 3, "There should be 1 event emitted from the contribution!");
      assert.strictEqual(result.logs[0].event, expectedEventContribution, `The event emitted was ${result.logs[0].event} instead of ${expectedEventContribution}`);
      assert.strictEqual(result.logs[2].event, expectedEventComplete, `The event emitted was ${result.logs[0].event} instead of ${expectedEventComplete}`);
    });



  })


});
