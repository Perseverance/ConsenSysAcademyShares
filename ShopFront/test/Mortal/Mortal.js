const ShopFront = artifacts.require("./ShopFront.sol");
const ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFrontMortal', function(accounts) {

  let SFInstance;
  let SFTPOInstance;

  const _owner = accounts[0];
  const _notOwner = accounts[1];
  const _sponsor = accounts[2];
  const _shippingOracle = accounts[5];

  const transactionValue = web3.toWei(1, "ether");

  beforeEach(async function() {
    SFTPOInstance = await ShopFrontTokenPriceOracle.new();
    SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
  })

  it("should return eth correctly", async function() {

    await web3.eth.sendTransaction({ from: _sponsor, to: SFInstance.address, value: transactionValue });
    let balanceAfterSend = await web3.eth.getBalance(_sponsor);

    await SFInstance.closeContract({ from: _sponsor });
    let balanceAfterClose = await web3.eth.getBalance(_sponsor);

    assert.isAbove(balanceAfterClose.toNumber(), balanceAfterSend.toNumber(), "The owner was funded after closing!");
  });

  it("should not be able to close not being an sponsor", async function() {
    await expectThrow(SFInstance.closeContract.call({ from: _notOwner }))
  });

  it("should emit event for contract destroyed", async function() {
    const expectedEvent = 'LogContractDestroyed';
    let result = await SFInstance.closeContract({ from: _sponsor });
    assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the contract closure!");
    assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
  })

});
