const ShopFront = artifacts.require("./ShopFront.sol");
const ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFrontOwned', function(accounts) {

  let SFInstance;
  let SFTPOInstance;
  const _owner = accounts[0];
  const _notOwner = accounts[1];
  const _sponsor = accounts[2];
  const _shippingOracle = accounts[5];

  beforeEach(async function() {
    SFTPOInstance = await ShopFrontTokenPriceOracle.new();
    SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
  })

  describe("initialization", () => {
    it("should set the sponsor correctly", async function() {
      let sponsor = await SFInstance.sponsor.call();
      assert.strictEqual(sponsor, _sponsor, "Account sponsor differs from the actual sponsor");
    });
  })

  describe("changing sponsor", () => {
    it("should set the new sponsor correctly", async function() {
      await SFInstance.changeSponsor(_notOwner, { from: _sponsor });
      let sponsor = await SFInstance.sponsor.call();
      assert.strictEqual(sponsor, _notOwner, "Account owner differs from the one supplied");
    });

    it("should throw if non-sponsor tries to change", async function() {
      await expectThrow(SFInstance.changeSponsor.call(_notOwner, { from: _notOwner }));
    });

    it("should throw if tries to change to 0x0", async function() {
      await expectThrow(SFInstance.changeSponsor.call("0x0", { from: _sponsor }));
    });

    it("should throw if the same sponsor tries to change to himself", async function() {
      await expectThrow(SFInstance.changeSponsor.call(_sponsor, { from: _sponsor }));
    });

    it("should emit event on sponsor change", async function() {
      const expectedEvent = 'LogSponsorChanged';
      let result = await SFInstance.changeSponsor(_notOwner, { from: _sponsor });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the sponsor change!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });
  })

});
