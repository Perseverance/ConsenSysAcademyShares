const ShopFront = artifacts.require("./ShopFront.sol");
const ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFrontShopReceiptIssuer', function(accounts) {

  let SFInstance;
  let SFTPOInstance;

  const _owner = accounts[0];
  const _notOwner = accounts[1];
  const _sponsor = accounts[2];
  const _shippingOracle = accounts[5];

  const dummyProduct = {
    price: 100,
    stock: 30,
    meta: "VeryCoolProduct__Description"
  }

  const transactionValue = web3.toWei(1, "ether");


  describe("initialization tests", () => {
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
    })

    it("should create the active receipts with 1 element", async function() {
      let activeReceipts = (await SFInstance.getReceiptCount.call()).toNumber();
      assert.equal(activeReceipts, 1, "The count of active receipts should be 1 after creation!");
    });

    it("should throw if try to access index 0", async function() {
      await expectThrow(SFInstance.activeReceipts.call(0));
    });
  });

  describe("issuing receipts", () => {
    let productId;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
      productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })
    })

    it("should issue receipts on buy", async function() {
      let activeReceiptsBefore = await SFInstance.getReceiptCount.call();

      await SFInstance.buyProduct(productId, { value: dummyProduct.price });
      let activeReceiptsAfter = await SFInstance.getReceiptCount.call();

      assert(activeReceiptsAfter.eq(activeReceiptsBefore.plus(1)), "The receipts count has not incremented");

    })

    it("should set correctly the mapping receiptId=>index", async function() {
      let receiptId = (await SFInstance.buyProduct.call(productId, { value: dummyProduct.price }))[1];

      await SFInstance.buyProduct(productId, { value: dummyProduct.price });

      let mappingIndexPointer = await SFInstance.receiptIdToIndex.call(receiptId.toNumber());

      assert(mappingIndexPointer.gt(0), "The receiptId points to 0 in the receiptId => index mapping");
    })

    it("has storred the receipt successfully", async function() {
      let receiptId = (await SFInstance.buyProduct.call(productId, { value: dummyProduct.price }))[1];

      await SFInstance.buyProduct(productId, { value: dummyProduct.price });

      let mappingIndexPointer = await SFInstance.receiptIdToIndex.call(receiptId.toNumber());

      let storedReceipt = await SFInstance.activeReceipts.call(mappingIndexPointer.toNumber());
      assert(receiptId.eq(storedReceipt[0]), "The receipt id stored is different from the one returned");
      assert.equal(_owner, storedReceipt[1], "The receipt receiver stored is different from the one returned");
      assert(storedReceipt[2].eq(productId), "The product id stored is different from the one returned");
    })

    it("should emit event on receipt created", async function() {
      const expectedEvent = 'LogReceiptCreated';

      let result = await SFInstance.buyProduct(productId, { value: dummyProduct.price });
      assert.lengthOf(result.logs, 2, "There should be 2 events emitted from the receipt creation!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    })

  })

  describe("fulfilling receipts", () => {
    let productId;
    let receiptId;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);

      productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })

      receiptId = (await SFInstance.buyProduct.call(productId, { value: dummyProduct.price }))[1].toNumber();
      await SFInstance.buyProduct(productId, { value: dummyProduct.price });
    })

    it("should throw if non-shipping-oracle tries to fulfill shipment", async function() {
      await expectThrow(SFInstance.fulfillShipment(receiptId, { from: _sponsor }));
    })

    it("should be able to fulfill shipment", async function() {
      let activeReceiptsBefore = await SFInstance.getReceiptCount.call();

      await SFInstance.fulfillShipment(receiptId, { from: _shippingOracle });

      let activeReceiptsAfter = await SFInstance.getReceiptCount.call();
      assert(activeReceiptsAfter.eq(activeReceiptsBefore.sub(1)), "The receipt count has not decreased.")

      let mappingIndexPointer = await SFInstance.receiptIdToIndex.call(receiptId);
      assert(mappingIndexPointer.eq(0), "The receiptId is no longer active in the mapping");
    })

    it("should emit event on shipment fulfilled", async function() {
      const expectedEvent = 'LogReceiptFulFilled';

      let result = await SFInstance.fulfillShipment(receiptId, { from: _shippingOracle });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the fulfilling shipment!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    })
  })

});
