const ShopFront = artifacts.require("./ShopFront.sol");
const ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFrontPausable', function(accounts) {

  let SFInstance;
  let SFTPOInstance;

  const _owner = accounts[0];
  const _notOwner = accounts[1];
  const _sponsor = accounts[1];
  const _shippingOracle = accounts[5];

  const transactionValue = web3.toWei(1, "ether");

  beforeEach(async function() {
    SFTPOInstance = await ShopFrontTokenPriceOracle.new();
    SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
  })

  it("can pause the contract", async function() {

    await SFInstance.pause();
    let isPaused = await SFInstance.isPaused.call();

    assert.isTrue(isPaused, "The contract is not paused after calling pause()");
  });

  it("can resume the contract", async function() {
    await SFInstance.pause();

    await SFInstance.resume();
    isPaused = await SFInstance.isPaused.call();

    assert.isFalse(isPaused, "The contract is not resumed after calling resume()");
  });

  it("throws if non-owner tries to pause or resume", async function() {
    await expectThrow(SFInstance.pause({ from: _notOwner }));

    await SFInstance.pause();

    await expectThrow(SFInstance.resume({ from: _notOwner }))
  })

  it("should emit event for pause", async function() {
    const expectedEvent = 'LogContractPaused';

    let result = await SFInstance.pause();

    assert.lengthOf(result.logs, 1, "There should be 1 event emitted from calling pause()!");
    assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
  })

  it("should emit event for resume", async function() {
    const expectedEvent = 'LogContractResumed';

    await SFInstance.pause();
    let result = await SFInstance.resume();

    assert.lengthOf(result.logs, 1, "There should be 1 event emitted from calling resume()!");
    assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
  })

});
