const ShopFront = artifacts.require("./ShopFront.sol");
const ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFrontOwned', function(accounts) {

  let SFInstance;
  let SFTPOInstance;
  const _owner = accounts[0];
  const _notOwner = accounts[1];
  const _sponsor = accounts[2];
  const _shippingOracle = accounts[5];

  beforeEach(async function() {
    SFTPOInstance = await ShopFrontTokenPriceOracle.new();
    SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
  })

  describe("initialization", () => {
    it("should set the owner correctly", async function() {
      let owner = await SFInstance.owner.call();
      assert.strictEqual(owner, _owner, "Account owner differs from the actual owner");
    });
  })

  describe("changing owner", () => {
    it("should set the new owner correctly", async function() {
      await SFInstance.changeOwner(_notOwner);
      let owner = await SFInstance.owner.call();
      assert.strictEqual(owner, _notOwner, "Account owner differs from the one supplied");
    });

    it("should throw if non-owner tries to change", async function() {
      await expectThrow(SFInstance.changeOwner.call(_notOwner, { from: _notOwner }));
    });

    it("should throw if tries to change to 0x0", async function() {
      await expectThrow(SFInstance.changeOwner.call("0x0"));
    });

    it("should throw if the same owner tries to change to himself", async function() {
      await expectThrow(SFInstance.changeOwner.call(_owner));
    });

    it("should emit event on owner change", async function() {
      const expectedEvent = 'LogOwnerChanged';
      let result = await SFInstance.changeOwner(_notOwner);
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the owner change!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });
  })



});
