const ShopFront = artifacts.require("./ShopFront.sol");
const StandardToken = artifacts.require("./StandardToken.sol");
const ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFront', function(accounts) {

  let SFInstance;
  let SFTPOInstance;

  const _owner = accounts[0];
  const _notOwner = accounts[1];
  const _sponsor = accounts[2];
  const _shippingOracle = accounts[5];

  const _dummyPriceEnough = 100;
  const _dummyPriceNotEnough = _dummyPriceEnough / 4;

  const dummyProduct = {
    price: _dummyPriceEnough,
    stock: 30,
    meta: "VeryCoolProduct__Description"
  }

  const transactionValue = web3.toWei(1, "ether");

  describe("adding product", () => {
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
    })

    it("should be able to add product", async function() {
      let productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })

      let product = await SFInstance.products.call(productId);
      assert(product[0].eq(dummyProduct.price), "The product price stored is different from the one sent");
      assert(product[1].eq(dummyProduct.stock), "The product stock stored is different from the one sent");
      assert(web3.toAscii(product[2]).includes(dummyProduct.meta), "The product meta stored is different from the one returned");
      assert.isTrue(product[3], "The product is not active");
    });

    it("should increase product count", async function() {
      let productsCountBefore = await SFInstance.getProductsCount.call();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor });
      let productsCountAfter = await SFInstance.getProductsCount.call();

      assert(productsCountAfter.gt(productsCountBefore), "The products counter has not changed");
    });

    it("should add the new product in the activeProductIds array", async function() {
      let productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1];
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor });

      let arrayProductId = await SFInstance.activeProductIds.call(0);

      assert(productId.eq(productId), "The productId was stored correctly");
    });

    it("should return new productId for new products", async function() {
      let productIdBefore = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1];
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })

      let productIdAfter = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1];

      assert(productIdAfter.eq(productIdBefore.plus(1)), "The system returns one and the same productIds");
    });

    it("should throw if non-sponsor tries to add product", async function() {
      await expectThrow(SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _notOwner }));
    })

    it("should throw if no price", async function() {
      await expectThrow(SFInstance.addProduct.call(0, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }));
    });

    it("should throw if no stock", async function() {
      await expectThrow(SFInstance.addProduct.call(dummyProduct.price, 0, dummyProduct.meta, { from: _sponsor }));
    });

    it("should emit event on product added", async function() {
      const expectedEvent = 'LogProductCreated';
      let result = await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the product add!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });
  });

  describe("adding stock", () => {
    let productId;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
      productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })
    })

    it("should be able to add stock", async function() {
      await SFInstance.addStock(productId, dummyProduct.stock, { from: _sponsor });
      let product = await SFInstance.products.call(productId);
      assert(product[1].eq(dummyProduct.stock * 2), "The product stock has not increased");
    });
    it("should throw if non sponsor adds stock", async function() {
      await expectThrow(SFInstance.addStock.call(productId, dummyProduct.stock, { from: _notOwner }));
    });
    it("should throw if trying to add 0", async function() {
      await expectThrow(SFInstance.addStock.call(productId, 0, { from: _sponsor }));
    });
    it("should emit event for adding stock", async function() {
      const expectedEvent = 'LogStockChanged';
      let result = await SFInstance.addStock(productId, dummyProduct.stock, { from: _sponsor });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from adding stock!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });
  })

  describe("changing price", () => {
    let productId;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
      productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })
    })

    it("should be able to change price", async function() {
      await SFInstance.changePrice(productId, dummyProduct.price * 2, { from: _sponsor });
      let product = await SFInstance.products.call(productId);
      assert(product[0].eq(dummyProduct.price * 2), "The product price has not changed");
    });
    it("should throw if non sponsor changes price", async function() {
      await expectThrow(SFInstance.changePrice.call(productId, dummyProduct.price, { from: _notOwner }));
    });
    it("should throw if trying to add 0", async function() {
      await expectThrow(SFInstance.changePrice.call(productId, 0, { from: _sponsor }));
    });
    it("should emit event for changing price", async function() {
      const expectedEvent = 'LogPriceChanged';
      let result = await SFInstance.changePrice(productId, dummyProduct.price, { from: _sponsor });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from changing price!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });
  })

  describe("deactivating product", () => {
    let productId;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
      productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })
    })

    it("should be able to deactivate product", async function() {
      await SFInstance.deactivateProduct(productId, { from: _sponsor });
      let product = await SFInstance.products.call(productId);
      assert.isFalse(product[3], "The product was not deactivated");
    });

    it("should decrease product count", async function() {
      let productsCountBefore = await SFInstance.getProductsCount.call();
      await SFInstance.deactivateProduct(productId, { from: _sponsor });
      let productsCountAfter = await SFInstance.getProductsCount.call();

      assert(productsCountAfter.lt(productsCountBefore), "The products counter has not changed");
    });

    it("should update arrayIndex", async function() {
      let secondProductId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })
      let productBefore = await SFInstance.products.call(secondProductId);
      await SFInstance.deactivateProduct(productId, { from: _sponsor });
      let productAfter = await SFInstance.products.call(secondProductId);

      assert(productAfter[4].lt(productBefore[4]), "The product index has changed");
    })

    it("should throw if non sponsor tries to deactivate", async function() {
      await expectThrow(SFInstance.deactivateProduct.call(productId, { from: _notOwner }));
    });
    it("should emit event for deactivation", async function() {
      const expectedEvent = 'LogDeactivated';
      let result = await SFInstance.deactivateProduct(productId, { from: _sponsor });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from deactivation!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });
  })

  describe("buying product", () => {
    let productId;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
      productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })
    })

    it("should be able buy product", async function() {
      let result = await SFInstance.buyProduct.call(productId, { value: dummyProduct.price });
      assert.isTrue(result[0], "The product buy was unsuccesful");
      assert.isAbove(result[1].toNumber(), 0, "The receipt returned was 0 or less");
    });

    it("should decrease stock", async function() {
      await SFInstance.buyProduct(productId, { value: dummyProduct.price });
      let product = await SFInstance.products.call(productId);
      assert(product[1].eq(dummyProduct.stock - 1), "The product stock has not increased");
    })

    it("should throw if try to buy inactive product", async function() {
      await SFInstance.deactivateProduct(productId, { from: _sponsor });
      await expectThrow(SFInstance.buyProduct.call(productId, { value: dummyProduct.price }));
    });
    it("should throw if product is not in stock", async function() {
      productId = (await SFInstance.addProduct.call(dummyProduct.price, 1, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, 1, dummyProduct.meta, { from: _sponsor })
      await SFInstance.buyProduct(productId, { value: dummyProduct.price });
      await expectThrow(SFInstance.buyProduct.call(productId, { value: dummyProduct.price }));
    });

    it("should throw if value is less than price", async function() {
      await expectThrow(SFInstance.buyProduct.call(productId, { value: dummyProduct.price - 10 }));
    });
    it("should emit event for buying product", async function() {
      const expectedEvent = 'LogProductBought';
      let result = await SFInstance.buyProduct(productId, { value: dummyProduct.price });
      assert.lengthOf(result.logs, 2, "There should be 1 event emitted from buying product!");
      assert.strictEqual(result.logs[1].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });
  })

  describe("buying with tokens", () => {
    let productId;
    let tokenContract;
    const initialValue = 5000;
    const tokensPerWei = 10;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();

      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
      productId = (await SFInstance.addProduct.call(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, dummyProduct.stock, dummyProduct.meta, { from: _sponsor });

      tokenContract = await StandardToken.new(initialValue);
      await tokenContract.approve(SFInstance.address, dummyProduct.price * tokensPerWei);
      await SFTPOInstance.setTokenPrice(tokenContract.address, tokensPerWei);
    })

    it("should be able buy product with tokens", async function() {
      let result = await SFInstance.buyWithTokens.call(productId, tokenContract.address);
      assert.isTrue(result[0], "The product buy was unsuccesful");
      assert.isAbove(result[1].toNumber(), 0, "The receipt returned was 0 or less");
    });

    it("should decrease stock", async function() {
      await SFInstance.buyWithTokens(productId, tokenContract.address);
      let product = await SFInstance.products.call(productId);
      assert(product[1].eq(dummyProduct.stock - 1), "The product stock has not increased");
    })

    it("should throw try to buy inactive product", async function() {
      await SFInstance.deactivateProduct(productId, { from: _sponsor });
      await expectThrow(SFInstance.buyWithTokens.call(productId, tokenContract.address));
    });

    it("should throw if token allowance not enough", async function() {
      await tokenContract.approve(SFInstance.address, (dummyProduct.price * tokensPerWei) / 2);
      await expectThrow(SFInstance.buyWithTokens.call(productId, tokenContract.address));
    });

    it("should throw if not enough value", async function() {
      tokenContract = await StandardToken.new(dummyProduct.price);
      await tokenContract.approve(SFInstance.address, dummyProduct.price * tokensPerWei);
      await expectThrow(SFInstance.buyWithTokens.call(productId, tokenContract.address));
    });

    it("should throw if product is not in stock", async function() {
      productId = (await SFInstance.addProduct.call(dummyProduct.price, 1, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(dummyProduct.price, 1, dummyProduct.meta, { from: _sponsor })
      await SFInstance.buyProduct(productId, { value: dummyProduct.price });
      await expectThrow(SFInstance.buyWithTokens.call(productId, tokenContract.address));
    });


    it("should emit event for buying product", async function() {
      const expectedEvent = 'LogProductBought';
      let result = await SFInstance.buyWithTokens(productId, tokenContract.address);
      assert.lengthOf(result.logs, 2, "There should be 1 event emitted from the buying product with tokens!");
      assert.strictEqual(result.logs[1].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });
  })

  describe("withdrawing funds", async function() {
    let contractBalance = transactionValue;
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
      productId = (await SFInstance.addProduct.call(transactionValue, dummyProduct.stock, dummyProduct.meta, { from: _sponsor }))[1].toNumber();
      await SFInstance.addProduct(transactionValue, dummyProduct.stock, dummyProduct.meta, { from: _sponsor })
      await SFInstance.buyProduct(productId, { value: transactionValue });
    })

    it("should allow to withdraw funds from the contract", async function() {
      let balanceBeforeWithdrawal = await web3.eth.getBalance(_sponsor);
      await SFInstance.withdrawFunds(contractBalance, { from: _sponsor });
      let balanceAfterWithdrawal = await web3.eth.getBalance(_sponsor);
      assert(balanceAfterWithdrawal.gt(balanceBeforeWithdrawal), "The owner balance has not increased!");

    });
    it("should throw if try to get more funds than contract balance", async function() {
      await expectThrow(SFInstance.withdrawFunds.call(contractBalance * 2, { from: _sponsor }));
    });
    it("should throw if non-sponsor tries to withdraw", async function() {
      await expectThrow(SFInstance.withdrawFunds.call(contractBalance, { from: _notOwner }));
    });
    it("should emit event on withdrawal", async function() {
      const expectedEvent = 'LogFundsWithdrawn';
      let result = await SFInstance.withdrawFunds(contractBalance, { from: _sponsor });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the withdrawal!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    });
  })

});
