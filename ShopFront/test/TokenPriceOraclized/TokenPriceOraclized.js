const ShopFront = artifacts.require("./ShopFront.sol");
const ShopFrontTokenPriceOracle = artifacts.require("./ShopFrontTokenPriceOracle.sol");
const expectThrow = require('../util').expectThrow;

contract('ShopFrontTokenPriceOraclized', function(accounts) {

  let SFInstance;
  let SFTPOInstance;

  const _owner = accounts[0];
  const _notOwner = accounts[1];
  const _sponsor = accounts[2];
  const _shippingOracle = accounts[5];

  const transactionValue = web3.toWei(1, "ether");

  describe("initialization", () => {
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
    })

    it("should set the oracle on init", async function() {

      let oracle = await SFInstance.priceOracle.call();

      assert.strictEqual(oracle, SFTPOInstance.address, "The oracle was not set correctly on init of the contract!");
    });

  })

  describe("setting oracle", () => {
    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
    })

    it("should be able to change oracle", async function() {
      await SFInstance.setPriceOracle(_notOwner, { from: _sponsor });

      let oracle = await SFInstance.priceOracle.call();

      assert.strictEqual(oracle, _notOwner, "The oracle was not changed to the new oracle!");
    })

    it("should not be able to change oracle from non-sponsor", async function() {
      await expectThrow(SFInstance.setPriceOracle(_notOwner, { from: _notOwner }))
    });

    it("should emit event for oracle set", async function() {
      const expectedEvent = 'LogPriceOracleSet';
      let result = await SFInstance.setPriceOracle(_notOwner, { from: _sponsor });
      assert.lengthOf(result.logs, 1, "There should be 1 event emitted from the contract closure!");
      assert.strictEqual(result.logs[0].event, expectedEvent, `The event emitted was ${result.logs[0].event} instead of ${expectedEvent}`);
    })
  })

  describe("working with prices", async function() {
    const _dummyTokenAddress = "0x112233";
    const _invalidDummyTokenAddress = "0x332211";
    const _baseTokensPerWei = 100;

    beforeEach(async function() {
      SFTPOInstance = await ShopFrontTokenPriceOracle.new();
      SFTPOInstance.setTokenPrice(_dummyTokenAddress, _baseTokensPerWei);
      SFInstance = await ShopFront.new(_sponsor, _shippingOracle, SFTPOInstance.address);
    })

    it("should be able to get the price", async function() {
      let ratePerWei = await SFInstance.getTokensPerWeiRate.call(_dummyTokenAddress);
      assert(ratePerWei.eq(_baseTokensPerWei), "The rate set was not returned correctly!");
    })

    it("should throw if the token is not set", async function() {
      await expectThrow(SFInstance.getTokensPerWeiRate.call(_invalidDummyTokenAddress))
    })

  })

});
