pragma solidity ^0.4.15;
contract Owned {

    event LogOwnerChanged(address oldOwner, address newOwner);

    address public owner;

    modifier onlyowner() {
        require(msg.sender == owner);
        _;
    }

    function Owned() {
        owner = msg.sender;
    }

    function changeOwner(address _newOwner) public onlyowner {
        require(_newOwner != address(0x0));
        require(_newOwner != owner);

        address oldOwner = owner;
        owner = _newOwner;
        LogOwnerChanged(oldOwner, owner);
    }
}
