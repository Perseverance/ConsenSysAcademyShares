pragma solidity ^0.4.15;

contract CoBuy {

    event LogCoBuyStarted(uint partialReceiptId, uint productId, address contributor, uint contribution);
    event LogCoBuyContributed(uint partialReceiptId, uint productId, address contributor, uint contribution);
    event LogCoBuyFinished(uint partialReceiptId, uint productId, uint receiptId);

    uint public coBuyCounter;

    mapping(uint => PartialReceipt) public partialReceipts;

    struct PartialReceipt {
        uint partialReceiptId;
        uint valueAccumulated;
        uint productId;
        mapping(address => uint) contributions;
        bool isActive;
    }

    modifier isCoBuyActive(uint partialReceiptId) {
        require(partialReceipts[partialReceiptId].isActive);
        _;
    }


    function coBuy(uint productId) payable returns(uint partialReceiptId) {
        require(msg.value > 0);
        require(isProductAvailable(productId));

        uint productPrice = getProductPrice(productId);
        assert(productPrice > 0);
        require(msg.value < productPrice);


        partialReceiptId = ++coBuyCounter;
        partialReceipts[partialReceiptId] = PartialReceipt(
            {
                partialReceiptId:partialReceiptId,
                valueAccumulated: msg.value,
                productId: productId,
                isActive: true
            });
        partialReceipts[partialReceiptId].contributions[msg.sender] += msg.value;

        LogCoBuyStarted(partialReceiptId, productId, msg.sender, msg.value);

        return partialReceiptId;
    }

    function contributeToCoBuy(uint partialReceiptId) payable isCoBuyActive(partialReceiptId)
        returns(bool success, bool coBuyComplete, uint receiptId) {

        require(msg.value > 0);
        require(isProductAvailable(partialReceipts[partialReceiptId].productId));

        uint productPrice = getProductPrice(partialReceipts[partialReceiptId].productId);
        assert(productPrice > 0);

        partialReceipts[partialReceiptId].valueAccumulated += msg.value;
        partialReceipts[partialReceiptId].contributions[msg.sender] += msg.value;

        coBuyComplete = partialReceipts[partialReceiptId].valueAccumulated >= productPrice;

        LogCoBuyContributed(partialReceiptId, partialReceipts[partialReceiptId].productId, msg.sender, msg.value);

        if(coBuyComplete) {
						partialReceipts[partialReceiptId].isActive = false;
            receiptId = purchase(partialReceipts[partialReceiptId].productId);
            LogCoBuyFinished(partialReceiptId, partialReceipts[partialReceiptId].productId, receiptId);
        }

        return (true, coBuyComplete, receiptId);
    }

    function isProductAvailable(uint productId) internal constant returns(bool);

    function getProductPrice(uint productId) internal constant returns(uint price);

    function purchase(uint productId) internal returns(uint receiptId);
}
