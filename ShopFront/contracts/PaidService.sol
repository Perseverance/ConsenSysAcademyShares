pragma solidity ^0.4.15;
import "./Owned.sol";

contract PaidService is Owned {
    uint public serviceFee;
    uint public feesToBeCollected;

    event LogServiceFeeAccounted(address payer, uint amount);
    event LogServiceFeesCollected(uint amount);
    event LogServiceFeeChanged(uint fee);

    function PaidService(uint _serviceFee) {
        serviceFee = _serviceFee;
    }

    function changeServiceFee(uint _serviceFee) onlyowner {
        serviceFee = _serviceFee;
        LogServiceFeeChanged(serviceFee);
    }

    function collectFees() onlyowner {
        assert(feesToBeCollected <= this.balance);
        uint value = feesToBeCollected;
        feesToBeCollected = 0;
        owner.transfer(value);
        LogServiceFeesCollected(value);
    }

    modifier paidService {
        require(msg.value >= serviceFee);
        feesToBeCollected += msg.value;
        LogServiceFeeAccounted(msg.sender, serviceFee);
        _;
    }

}
