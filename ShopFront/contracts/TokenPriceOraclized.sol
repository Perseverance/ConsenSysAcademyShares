pragma solidity ^0.4.15;

import "./Sponsored.sol";
import "./TokenPriceOracle.sol";

contract TokenPriceOraclized is Sponsored {

    event LogPriceOracleSet(address oracle);

    address public priceOracle;

    function TokenPriceOraclized(address sponsor, address oracle) Sponsored(sponsor) {
			require(oracle != address(0x0));
			priceOracle = oracle;
    }

    function setPriceOracle(address newOracle) onlySponsor {
        require(newOracle != address(0x0));
        priceOracle = newOracle;
        LogPriceOracleSet(priceOracle);
    }

    function getTokensPerWeiRate(address token) public constant returns (uint tokensPerWei) {
        TokenPriceOracle oracle = TokenPriceOracle(priceOracle);
				tokensPerWei = oracle.getTokenPerWei(token);
				require(tokensPerWei > 0);
        return tokensPerWei;
    }

}
