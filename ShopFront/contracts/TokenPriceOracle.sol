pragma solidity ^0.4.15;

import "./Mortal.sol";

contract TokenPriceOracle is Mortal {

    mapping(address=>bool) availableTokens;
    mapping(address=>uint) tokensPerWei;

    function getTokenPerWei(address token) constant returns(uint valueInWei);

    function setTokenPrice(address token, uint valueInWei);

    function removeToken(address token);

    modifier tokenAvailable(address token) {
        require(availableTokens[token]);
        _;
    }
}
