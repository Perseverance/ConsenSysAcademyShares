pragma solidity ^0.4.15;

import "./ShopFront.sol";
import "./Mortal.sol";
import "./PaidService.sol";

contract ShopFrontHub is Owned, PaidService {

    event LogShopFrontCreated(address shop, address sponsor);
    event LogOwnerChanged(address shop, address newOwner);
    event LogContractDestroyed(address destroyer);

    address[] public shops;

    mapping(address => bool) public activeShops;

    modifier isShopActive(address shop) {
        require(activeShops[shop]);
        _;
    }

    function ShopFrontHub(uint _serviceFee) PaidService(_serviceFee) {

    }

    function createShop(address shippingOracle, address tokenOracle) payable paidService returns(address shop) {
        address shopAddress = new ShopFront(msg.sender, shippingOracle, tokenOracle);
        assert(!activeShops[shopAddress]);
        shops.push(shopAddress);
        activeShops[shopAddress] = true;
        LogShopFrontCreated(shopAddress, msg.sender);
        return shopAddress;
    }

    function pauseShop(address shop) onlyowner isShopActive(shop) returns(bool) {
        ShopFront s = ShopFront(shop);
        s.pause();
        return true;
    }

    function resumeShop(address shop) onlyowner isShopActive(shop) returns(bool) {
        ShopFront s = ShopFront(shop);
        s.resume();
        return true;
    }

    function changeShopOwner(address shop, address newOwner) onlyowner isShopActive(shop) returns(bool) {
        require(newOwner != address(0x0));
        require(newOwner != owner);

        ShopFront s = ShopFront(shop);
        s.changeOwner(newOwner);
        LogOwnerChanged(shop, newOwner);
        return true;
    }

    function closeContract() onlyowner {
        LogContractDestroyed(msg.sender);
        selfdestruct(msg.sender);
    }
    
}
