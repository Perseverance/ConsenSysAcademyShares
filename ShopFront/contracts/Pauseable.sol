pragma solidity ^0.4.15;

import "./Owned.sol";

contract Pauseable is Owned {

		event LogContractPaused();
		event LogContractResumed();

    bool public isPaused;

    function pause() public onlyowner {
        isPaused = true;
		LogContractPaused();
    }

    function resume() public onlyowner {
        isPaused = false;
		LogContractResumed();
    }

    modifier isNotPaused {
        require(!isPaused);
        _;
    }
}
