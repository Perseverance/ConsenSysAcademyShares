pragma solidity ^0.4.15;

import "./Sponsored.sol";
import "./Mortal.sol";
import "./Pauseable.sol";
import "./CoBuy.sol";
import "./ShopReceiptIssuer.sol";
import "./ShippingOraclized.sol";
import "./TokenPriceOraclized.sol";
import "./Token.sol";

contract ShopFront is Sponsored, Mortal, Pauseable, ShopReceiptIssuer, ShippingOraclized, TokenPriceOraclized, CoBuy {
    event LogProductCreated(uint productId, uint price, uint stock, bytes32 meta);
    event LogPriceChanged(uint productId, uint newPrice);
    event LogStockChanged(uint productId, uint newStock, uint stockTotal);
		event LogProductBought(uint productId);
    event LogDeactivated(uint productId);
		event LogFundsWithdrawn(uint value);

    uint private nextProductId = 0;

    struct Product {
        uint price;
        uint stock;
        bytes32 meta;
        bool isActive;
        uint arrayIndex;
    }

    mapping(uint => Product) public products;

    uint[] public activeProductIds;

    modifier onlyowner() {
        require(msg.sender == owner);
        _;
    }

    modifier productExists(uint productId) {
        require(productId < nextProductId);
        Product storage p = products[productId];
        require(p.isActive);
        _;
    }

    modifier isInStock(uint productId) {
        Product storage p = products[productId];
        require(p.stock > 0);
        _;
    }

    function ShopFront(address sponsor, address shippingOracle, address tokenPriceOracle) Sponsored(sponsor) ShippingOraclized(sponsor, shippingOracle) TokenPriceOraclized(sponsor, tokenPriceOracle) {

    }

    function getProductsCount() public isNotPaused constant returns(uint count) {
        count = activeProductIds.length;
    }

    function addProduct(uint price, uint stock, bytes32 meta) public onlySponsor isNotPaused returns(bool success, uint productId) {
        require(stock > 0);
        require(price > 0);

        productId = nextProductId;

        products[productId] = Product({price: price, stock: stock, meta: meta, isActive: true, arrayIndex: activeProductIds.length});
        activeProductIds.push(productId);
        nextProductId++;
        LogProductCreated(productId, price, stock, meta);
        return (true, productId);
    }

    function addStock(uint productId, uint stock) public onlySponsor isNotPaused productExists(productId)  returns (bool success) {
        require(stock > 0);

        products[productId].stock += stock;
        LogStockChanged(productId, stock, products[productId].stock);
        return true;
    }

    function changePrice(uint productId, uint newPrice) public onlySponsor isNotPaused productExists(productId)  returns (bool success) {
        require(newPrice > 0);

        products[productId].price = newPrice;

        LogPriceChanged(productId, newPrice);

        return true;
    }

    function deactivateProduct(uint productId) public onlySponsor isNotPaused productExists(productId) returns(bool success) {
        products[productId].isActive = false;

        uint switchIndex = products[productId].arrayIndex;
        assert(switchIndex < activeProductIds.length);

        uint arrayLen = activeProductIds.length;

        uint switchProductId = activeProductIds[arrayLen-1];
        products[switchProductId].arrayIndex = switchIndex;
        activeProductIds[switchIndex] = switchProductId;
        activeProductIds.length--;

        LogDeactivated(productId);
        return true;
    }

    function buyProduct(uint productId) isNotPaused productExists(productId) isInStock(productId) public payable returns(bool success, uint receiptId) {
        Product storage p = products[productId];

        require(msg.value >= p.price);

        receiptId = purchase(productId);

				LogProductBought(productId);

        return (true, receiptId);
    }

    function buyWithTokens(uint productId, address tokenAddress) isNotPaused productExists(productId) isInStock(productId)  public returns(bool success, uint receiptId)  {
        Product storage p = products[productId];
        Token token = Token(tokenAddress);

        uint tokensPerWei = TokenPriceOraclized.getTokensPerWeiRate(tokenAddress);
        assert(tokensPerWei > 0);
        uint productPriceInToken = p.price * tokensPerWei;
        uint tokenAllowance = token.allowance(msg.sender, this);

        require(tokenAllowance >= productPriceInToken);

        receiptId = purchase(productId);

        token.transferFrom(msg.sender, this, productPriceInToken);

				LogProductBought(productId);

        return (true, receiptId);
    }

    function isProductAvailable(uint productId) internal isNotPaused productExists(productId) isInStock(productId) constant returns(bool) {
        return true;
    }

    function getProductPrice(uint productId) internal isNotPaused productExists(productId) constant returns(uint price) {
        return products[productId].price;
    }

    function purchase(uint productId) internal isNotPaused productExists(productId) isInStock(productId) returns(uint receiptId) {

        products[productId].stock--;

        receiptId = addReceipt(msg.sender, productId);

        return receiptId;
    }

    function withdrawFunds(uint funds) onlySponsor isNotPaused returns(bool success) {
        require(this.balance >= funds);
        msg.sender.transfer(funds);
				LogFundsWithdrawn(funds);
        return true;
    }

    function fulfillShipment(uint receiptId) onlyShippingOracle {
        ShopReceiptIssuer.fulfillReceipt(receiptId);
    }

    function() payable isNotPaused onlySponsor {

    }

}
