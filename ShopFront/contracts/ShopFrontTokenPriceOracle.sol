pragma solidity ^0.4.15;

import "./TokenPriceOracle.sol";


//This normally would be much more complicated but it serves its purpose for now
contract ShopFrontTokenPriceOracle is TokenPriceOracle {

    function getTokenPerWei(address token) constant tokenAvailable(token) returns(uint valueInWei) {
        return tokensPerWei[token];
    }

    function setTokenPrice(address token, uint _tokensPerWei) onlyowner {
        availableTokens[token] = true;
        tokensPerWei[token] = _tokensPerWei;
    }

    function removeToken(address token) tokenAvailable(token) {
        availableTokens[token] = false;
    }
}
