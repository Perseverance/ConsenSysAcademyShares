pragma solidity ^0.4.15;

import "./Sponsored.sol";

contract Mortal is Sponsored {

    event LogContractDestroyed(address destroyer);

    function closeContract() onlySponsor {
        LogContractDestroyed(msg.sender);
        selfdestruct(msg.sender);
    }

}
