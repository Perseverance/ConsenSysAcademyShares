pragma solidity ^0.4.15;

contract ShopReceiptIssuer {

    event LogReceiptCreated(uint receiptId, address receiver, uint productId);
    event LogReceiptFulFilled(uint receiptId);

    uint receiptIdCounter = 0;

    struct Receipt {
        uint receiptId;
        address productReceiver;
        uint productId;
    }

    Receipt[] public activeReceipts;

    mapping(uint => uint) public receiptIdToIndex;

    function ShopReceiptIssuer() {
        //The index 0 should never be used as any uninitialised key of the mapping will point to 0
        activeReceipts.length = 1;
    }

    function getReceiptCount() constant returns(uint count) {
        return(activeReceipts.length);
    }

    function activeReceipts(uint index) constant returns(uint receiptId, address receiver, uint productId) {
        require(index > 0);
        return (activeReceipts[index].receiptId, activeReceipts[index].productReceiver, activeReceipts[index].productId);
    }

    function addReceipt(address receiver, uint productId) internal returns(uint receiptId) {
        receiptIdCounter++;
        activeReceipts.push(Receipt({receiptId:receiptIdCounter, productReceiver: receiver, productId: productId}));
        receiptIdToIndex[receiptIdCounter] = activeReceipts.length - 1;
        LogReceiptCreated(receiptIdCounter, receiver, productId);
        return receiptIdCounter;
    }

    function removeReceipt(uint receiptId) private {
        require(receiptId <= receiptIdCounter);
        uint switchIndex = receiptIdToIndex[receiptId];
        assert(switchIndex < activeReceipts.length);
        assert(switchIndex > 0);

        uint arrayLen = activeReceipts.length;
        if(arrayLen == 2) {
            activeReceipts.length = 1;
            receiptIdToIndex[receiptId] = 0;
            return;
        }

        Receipt memory toSwitch = activeReceipts[arrayLen-1];
        activeReceipts[switchIndex] = toSwitch;
        activeReceipts.length--;
        receiptIdToIndex[toSwitch.receiptId] = switchIndex;
        receiptIdToIndex[receiptId] = 0;
    }

    function fulfillReceipt(uint receiptId) internal {
        removeReceipt(receiptId);
        LogReceiptFulFilled(receiptId);
    }
}
