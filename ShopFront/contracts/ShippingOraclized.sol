pragma solidity ^0.4.15;

import "./Sponsored.sol";

contract ShippingOraclized is Sponsored {

    event LogOracleSet(address oracle);

    address public shippingOracle;

    function ShippingOraclized(address sponsor, address oracle) Sponsored(sponsor) {
			require(oracle != address(0x0));
			shippingOracle = oracle;
    }

    function setShippingOracle(address newOracle) onlySponsor {
        require(newOracle != address(0x0));
        shippingOracle = newOracle;
        LogOracleSet(shippingOracle);
    }

    modifier onlyShippingOracle() {
        require(msg.sender == shippingOracle);
        _;
    }
}
